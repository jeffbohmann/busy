package org.jbo.busy;

import com.atlassian.jira.rest.client.api.domain.Issue;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import org.controlsfx.glyphfont.FontAwesome;
import org.jbo.busy.file.ConfigFile;
import org.jbo.busy.file.IssuesFile;
import org.jbo.busy.file.YamlTranslator;
import org.jbo.busy.task.LoadIssuesTask;
import org.jbo.busy.task.LogWorkTask;
import org.jbo.busy.tools.Browser;
import org.jbo.busy.tools.IssueSystemException;
import org.jbo.busy.tools.JiraTools;
import org.jbo.busy.tools.Log;
import org.jbo.busy.tools.ui.*;
import org.jbo.busy.tools.ui.TablePane.Row;
import org.joda.time.DateTime;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;


public class IssuesArea extends MaskedPane {
    public IssuesArea(JiraTools jiraTools, YamlTranslator<ConfigFile> configYamlTranslator, YamlTranslator<IssuesFile> issuesYamlTranslator) {
        this.jiraTools = jiraTools;
        this.configYamlTranslator = configYamlTranslator;
        this.issuesYamlTranslator = issuesYamlTranslator;

        issuesGrid = new TablePane();
        issuesGrid.setHgap(5);
        issuesGrid.setVgap(7);
        issuesGrid.setPadding(new Insets(10, 3, 10, 3));

        issuesGrid.getColumnConstraints().addAll(
                //issue key
                new ColumnConstraints(80, USE_COMPUTED_SIZE, Double.MAX_VALUE, null, HPos.LEFT, false),
                //summary
                new ColumnConstraints(90, USE_PREF_SIZE, Double.MAX_VALUE, Priority.ALWAYS, HPos.LEFT, false),
                //hours logged today
                new ColumnConstraints(50, USE_COMPUTED_SIZE, Double.MAX_VALUE, null, HPos.LEFT, false),
                //time log buttons
                new ColumnConstraints(120, USE_COMPUTED_SIZE, Double.MAX_VALUE, null, HPos.LEFT, false),
                //row actions
                new ColumnConstraints(30, USE_COMPUTED_SIZE, Double.MAX_VALUE, null, HPos.RIGHT, false)
        );

        ScrollPane scrollPane = new ScrollPane(issuesGrid);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPane.setFitToWidth(true);
        setNodetoMask(scrollPane);
    }

    public void reload() {
        Busy.app().status("Loading issues...");
        IssuesFile issuesFile = issuesYamlTranslator.read();
        if(!precheckIssues(issuesFile)) {
            return;
        }

        ConfigFile configFile = configYamlTranslator.read();
        //clear out the grid first
        issuesGrid.delAll();

        //repopulate the issues grid from the issue system
        LoadIssuesTask loadIssuesTask = new LoadIssuesTask(issuesFile, jiraTools);
        bindWait(loadIssuesTask.progressProperty());
        loadIssuesTask.setOnSucceeded(event -> {
            lastIssueResult = loadIssuesTask.getValue();
            processResults(lastIssueResult, configFile);
        });
        Thread thread = new Thread(loadIssuesTask);
        thread.setDaemon(true);
        thread.start();
    }

    public void refresh() {

        IssuesFile issuesFile = issuesYamlTranslator.read();
        if(!precheckIssues(issuesFile)) {
            return;
        }

        ConfigFile configFile = configYamlTranslator.read();
        //clear out the grid first
        issuesGrid.delAll();
        //populate the grid
        processResults(lastIssueResult, configFile);
    }

    private boolean precheckIssues(IssuesFile issuesFile) {
        if(issuesFile == null || issuesFile.size() == 0) {
            //No issues to load
            //clear the list
            issuesGrid.delAll();
            String msg = "No issues to load.";
            Busy.app().status(msg);
            log.debug(msg);
            return false;
        }
        return true;
    }

    private void processResults(final List<LoadIssuesTask.Result> results, final ConfigFile configFile) {
        for (LoadIssuesTask.Result result : results) {
            if (result.failed) {
                addErrorRow(result.key, result.e, configFile);
            } else {
                addIssueRow(result.jiraIssue, result.times, configFile);
            }
        }
        hideWait();
        Busy.app().status("Issues loaded.");
    }

    private void addIssueRow(final Issue jiraIssue, List<Integer> times, final ConfigFile configFile) {

        final String baseUrl = configFile.getBaseUrl();
        final int fontSize = configFile.getFontSize();
        final String key = jiraIssue.getKey();

        Row row = new Row(key);
        row.cells(createKeyLabel(key, baseUrl, fontSize),
                createSummaryLabel(jiraIssue, key, baseUrl, fontSize),
                createTimeLoggedTodayLabel(jiraIssue, fontSize),
                createTimeLogButtons(jiraIssue, times, configFile),
                createRowActions(row.id));

        issuesGrid.add(row);
    }

    private Label createKeyLabel(String key, String baseUrl, int fontSize) {
        Label keyLabel = new Label(key);
        keyLabel.setFont(Font.font(fontSize));
        keyLabel.setTextFill(Color.MEDIUMBLUE);
        keyLabel.setCursor(Cursor.HAND);
        keyLabel.setOnMouseClicked(event -> viewIssue(baseUrl, key));
        return keyLabel;
    }

    private Label createSummaryLabel(Issue jiraIssue, String key, String baseUrl, int fontSize) {
        Label summary = new Label(jiraIssue.getSummary());
        summary.setWrapText(true);
        summary.setFont(Font.font(fontSize));
        summary.setTextFill(Color.MEDIUMBLUE);
        summary.setCursor(Cursor.HAND);
        summary.setOnMouseClicked(event -> viewIssue(baseUrl, key));
        return summary;
    }

    private Label createTimeLoggedTodayLabel(Issue jiraIssue, int fontSize) {
        //determine worklogs totals for today
        Label timeLoggedToday = new Label("(" + UITools.buildTimeText(getIssueMinutesToday(jiraIssue)) + ")");
        timeLoggedToday.setFont(Font.font(fontSize));
        return timeLoggedToday;
    }

    private HBox createTimeLogButtons(final Issue jiraIssue, List<Integer> times, ConfigFile configFile) {
        int fontSize = configFile.getFontSize();

        HBox rowButtons = new HBox(10);
        rowButtons.setAlignment(Pos.CENTER_LEFT);
        for (Integer time : times) {

            rowButtons.getChildren().add(createTimeLogButton(jiraIssue, time, fontSize));
        }
        return rowButtons;
    }

    private Button createTimeLogButton(Issue jiraIssue, int time, int fontSize) {
        String formattedTime = UITools.buildTimeText(time);
        Button button = ButtonBuilder.createTextButton(formattedTime, null, action -> {
            logTime(jiraIssue, time);
        });
        button.setMinWidth(55);
        button.setFont(Font.font(fontSize));
        return button;
    }

    private void logTime(Issue jiraIssue, int time) {

        //show the popup window
        TimeLogWindow window = new TimeLogWindow(jiraIssue, time);
        Optional<TimeLogWindow.Result> optionalResult = window.showAndWait();
        optionalResult.ifPresent(result -> {
            Busy.app().status("Logging time...");
            showWait();

            LogWorkTask logWorkTaskTask = new LogWorkTask(jiraTools, jiraIssue,
                    result.minutes, result.comment);
            logWorkTaskTask.setOnSucceeded(event -> {
                Busy.app().status("Logged " + UITools.buildTimeText(time) +
                        " to issue " + jiraIssue.getKey());
                hideWait();
            });
            logWorkTaskTask.setOnFailed(event -> {
                String msg = "Failed to log work.";
                Busy.app().status(msg);
                log.error(msg, logWorkTaskTask.getException());
                hideWait();
            });
            Thread thread = new Thread(logWorkTaskTask);
            thread.setDaemon(true);
            thread.start();
        });


    }

    private ButtonWithMenu createRowActions(UUID id) {
        MenuItem remove = new MenuItem("Remove", UITools.glyph(FontAwesome.Glyph.TRASH_ALT));
        remove.setOnAction(event -> removeIssue(id));
        ButtonWithMenu button = new ButtonWithMenu(
                UITools.glyph(FontAwesome.Glyph.ELLIPSIS_V),
                remove
        );
        return button;
    }

    private void viewIssue(String baseUrl, String issueKey) {
        Browser.open(baseUrl + "/browse/" + issueKey, Busy.app());
    }

    private void removeIssue(UUID id) {
        //remove from the grid
        Row row = issuesGrid.del(id);

        //and remove from the file
        IssuesFile issuesFile = issuesYamlTranslator.read();
        for(Iterator<IssuesFile.Issue> it = issuesFile.iterator(); it.hasNext();) {
            if(it.next().key.equals(row.key)) {
                it.remove();
                break;
            }
        }
        issuesYamlTranslator.write(issuesFile);

        //now reload the issues for the grid
        reload();
    }

    private void addErrorRow(final String key, IssueSystemException e, ConfigFile configFile) {
        final int fontSize = configFile.getFontSize();

        Label keyLabel = new Label(key);
        keyLabel.setFont(Font.font(fontSize));
        keyLabel.setTextFill(Color.INDIANRED);

        Label errorLabel = new Label("ERROR: " + e.why() + " " + e.getMessage());
        errorLabel.setFont(Font.font(fontSize));
        errorLabel.setTextFill(Color.INDIANRED);
        errorLabel.setWrapText(true);

        issuesGrid.add(new Row(key).cells(keyLabel, errorLabel));
    }

    private int getIssueMinutesToday(Issue jiraIssue) {
        DateTime now = new DateTime(System.currentTimeMillis());
        DateTime startOfDay = new DateTime(now.year().get(),
                now.monthOfYear().get(), now.dayOfMonth().get(), 0, 0, 0, 0);
        DateTime endOfDay = new DateTime(now.year().get(),
                now.monthOfYear().get(), now.dayOfMonth().get(), 23, 59, 59, 999);

        final AtomicInteger minutesToday = new AtomicInteger(0);
        jiraIssue.getWorklogs().forEach(worklog -> {

            DateTime loggedDate = worklog.getStartDate();
            if(loggedDate.isAfter(startOfDay.toInstant()) &&
                loggedDate.isBefore(endOfDay.toInstant())) {

                minutesToday.addAndGet(worklog.getMinutesSpent());
            }
        });
        return minutesToday.get();
    }

    private List<LoadIssuesTask.Result> lastIssueResult;
    private JiraTools jiraTools;
    private TablePane issuesGrid;
    private YamlTranslator<ConfigFile> configYamlTranslator;
    private YamlTranslator<IssuesFile> issuesYamlTranslator;
    private final static Log log = Log.get();
}
