package org.jbo.busy;

import javafx.application.Platform;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import org.jbo.busy.tools.IssueSystemException;
import org.jbo.busy.tools.JiraTools;
import org.jbo.busy.tools.ui.ActionHandler;


public class ActionToolbar extends FlowPane {

    public enum Action {
        ISSUE_ENTERED;
    }
    public ActionToolbar(JiraTools jiraTools) {
        this.jiraTools = jiraTools;
        addIssueField();
    }

    public ActionToolbar on(Action action, EventHandler<Event> handler) {
        actionHandler.register(action, handler);
        return this;
    }

    private void addIssueField() {
        final TextField issueField = new TextField();
        issueField.setPromptText("Add an Issue");
        issueField.setOnKeyPressed(event -> {
            if(event.getCode() == KeyCode.ENTER) {
                try {
                    jiraTools.getIssue(issueField.getText(), issue -> {
                        //get back to the FX app thread
                        Platform.runLater(() -> {
                            if (issue != null) {
                                actionHandler.notify(Action.ISSUE_ENTERED, new Event(issue, null, EventType.ROOT));
                                issueField.clear();
                            } else {
                                issueField.setBackground(new Background(new BackgroundFill(Color.LIGHTCORAL, null, null)));
                            }
                        });
                        return null;
                    });
                }
                catch(IssueSystemException e) {
                    Busy.app().handleIssueSystemException(e);
                    issueField.setBackground(new Background(new BackgroundFill(Color.LIGHTCORAL, null, null)));
                }
            }
            else {
                //reset the field color
                issueField.setBackground(new Background(new BackgroundFill(Color.WHITE, null, null)));
            }
        });
        getChildren().add(issueField);
    }

    private ActionHandler<Action> actionHandler = new ActionHandler<>();
    private final JiraTools jiraTools;
}
