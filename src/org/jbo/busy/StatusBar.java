package org.jbo.busy;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Duration;
import org.controlsfx.control.PopOver;

public class StatusBar extends BorderPane {

    public StatusBar() {
        setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, null, null)));
        setEffect(new DropShadow(10, Color.BLACK));
        setHeight(20);

        status = new Label();
        status.setAlignment(Pos.BOTTOM_LEFT);
        status.setFont(Font.font(12));
        setLeft(status);

        user = new Label();
        user.setFont(Font.font(16));
        user.setAlignment(Pos.BOTTOM_RIGHT);
        setRight(user);
    }

    public void setStatus(String message) {
        status.setText(message);
    }

    public void updateUserInfo(String userName, String url) {
        user.setText(userName);
        if(url != null) {
            ImageView image = new ImageView(new Image(url, false));
            user.setGraphic(image);
            PopOver bigView = new PopOver(new ImageView(url.replace("&s=20", "&s=128")));

            user.setOnMouseEntered(event -> {
                bigView.show(user);
            });
            user.setOnMouseExited(event -> {
                bigView.hide(Duration.millis(250));
            });
        }
    }

    private Label status;
    private Label user;
}
