package org.jbo.busy.tools;

import com.atlassian.jira.rest.client.api.RestClientException;
import net.rcarz.jiraclient.JiraException;


public class IssueSystemException extends Exception {
    public enum Reason {
        UNKNOWN,
        AUTH_FAILED_401,
        FORBIDDEN_403,
        INVALID_URI
    }

    public static IssueSystemException forReason(String msg, Exception e, Reason reason) {
        return new IssueSystemException(msg + e.getMessage(), e, reason);
    }

    public static IssueSystemException fromCause(String msg, JiraException e) {
        return fromCause(msg, new RestClientException(e));
    }

    public static IssueSystemException fromCause(String msg, RestClientException e) {

        Reason reason = Reason.UNKNOWN;
        if(e.getStatusCode().isPresent()) {
            switch(e.getStatusCode().get()) {
                case 403:
                    reason = Reason.FORBIDDEN_403;
                    break;
                case 401:
                    reason = Reason.AUTH_FAILED_401;
            }
        }
        return new IssueSystemException(msg + e.getCause().getMessage(), e, reason);
    }

    public Reason why() {
        return reason;
    }

    private IssueSystemException(String msg, Throwable e, Reason reason) {
        super(msg, e);
        this.reason = reason;
    }

    private Reason reason;
}
