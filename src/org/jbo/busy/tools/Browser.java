package org.jbo.busy.tools;

import com.sun.deploy.uitoolkit.impl.fx.HostServicesFactory;
import com.sun.javafx.application.HostServicesDelegate;
import javafx.application.Application;


public class Browser {
    public static void open(String url, Application application) {
        HostServicesDelegate hostServices = HostServicesFactory.getInstance(application);
        hostServices.showDocument(url);
    }
}
