package org.jbo.busy.tools.ui;

import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

public class UITools {

    public static String buildTimeText(Integer time) {
        String timeText;
        if(time >= 60) {
            int hours = time / 60;
            int minutes = time % 60;
            timeText = hours + "h" + (minutes != 0 ? " " + minutes + "m" : "");
        }
        else {
            timeText = time.toString() + "m";
        }
        return timeText;
    }

    public static Glyph glyph(FontAwesome.Glyph glyph) {
        return font.create(glyph);
    }

    private static final GlyphFont font = GlyphFontRegistry.font("FontAwesome");
}
