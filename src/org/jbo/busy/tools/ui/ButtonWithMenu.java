package org.jbo.busy.tools.ui;

import javafx.event.ActionEvent;
import javafx.geometry.Side;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import org.controlsfx.glyphfont.Glyph;


public class ButtonWithMenu extends Button {

    public ButtonWithMenu(Glyph icon, MenuItem... menuItems) {
        this.setGraphic(icon);
        menu.getItems().addAll(menuItems);
        this.setOnAction(this::onClick);
    }

    public void onClick(ActionEvent event) {
        menu.show(this, Side.BOTTOM, 0, 0);
    }

    private ContextMenu menu = new ContextMenu();
}
