package org.jbo.busy.tools.ui;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import javafx.event.Event;
import javafx.event.EventHandler;

/**
 * Utility to map actions to one or more event handlers and allow registration and notification for listeners.
 */
public class ActionHandler<T extends Enum> {

    //TODO: add support for setting up delegation so the intermediate class (the one with the ActionHandler),
    //TODO: does not need to set up a listener and hand the event back to the real listener.
    //TODO: Maybe, "registerEventer" or "registerEventSource", which would be listened for in here and would call notify automatically.

    public ActionHandler<T> register(T action, EventHandler<Event> handler) {
        handlers.put(action, handler);
        return this;
    }

    public ActionHandler<T> notify(T action, Event event) {
        for(EventHandler<Event> handler : handlers.get(action)) {
            handler.handle(event);
        }
        return this;
    }

    private Multimap<T, EventHandler<Event>> handlers = ArrayListMultimap.create();
}
