package org.jbo.busy.tools.ui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import org.controlsfx.glyphfont.Glyph;

/**
 * 2 Buttons arranged how you like
 */
public class DualButton extends BorderPane {


    public static class Build {
        public static DualButton upDown(
                Glyph upIcon, String upTooltip, EventHandler<ActionEvent> upHandler,
                Glyph downIcon, String downTooltip, EventHandler<ActionEvent> downHandler) {

            return new DualButton(Orient.VERT,
                    ButtonBuilder.createImageButton(upIcon, upTooltip, upHandler),
                    ButtonBuilder.createImageButton(downIcon, downTooltip, downHandler));
        }

        public static DualButton leftRight(
                Glyph leftIcon, String leftTooltip, EventHandler<ActionEvent> leftHandler,
                Glyph rightIcon, String rightTooltip, EventHandler<ActionEvent> rightHandler) {

            return new DualButton(Orient.HORZ,
                    ButtonBuilder.createImageButton(leftIcon, leftTooltip, leftHandler),
                    ButtonBuilder.createImageButton(rightIcon, rightTooltip, rightHandler));
        }
    }

    private DualButton(Orient orient, Button one, Button two) {
        switch(orient) {
            case HORZ:
                setLeft(one);
                setRight(two);
                break;
            case VERT:
                setTop(one);
                setBottom(two);
                break;
        }
    }

    private enum Orient {VERT, HORZ};
}
