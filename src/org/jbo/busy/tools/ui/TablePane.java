package org.jbo.busy.tools.ui;


import javafx.scene.Node;
import javafx.scene.layout.GridPane;

import java.util.*;
import java.util.stream.Collectors;

public class TablePane extends GridPane {

    public TablePane add(Row row) {
        rows.add(row);
        addRow(rows.size(), getNodesForRow(row).toArray(new Node[row.cells.size()]));
        return this;
    }

    public TablePane del(Row row) {
        rows.remove(row);
        removeUnderlyingNodes(row);
        return this;
    }

    public Row del(UUID id) {
        Row toDelete = null;
        for(Row row : rows) {
            if(row.id.equals(id)) {
                toDelete = row;
                break;
            }
        }
        del(toDelete);
        return toDelete;
    }

    public TablePane del(int index) {
        Row row = rows.get(index);
        del(row);
        return this;
    }

    public TablePane delAll() {
        for(Iterator<Row> it = rows.iterator(); it.hasNext();) {
            Row row = it.next();
            //remove the cells from the row (and the grid)
            removeUnderlyingNodes(row);
            //remove the row from the tracked collection
            it.remove();
        }
        return this;
    }

    public TablePane move(int fromPosition, int toPosition) {
        Row row = rows.remove(fromPosition);

        //remove all the cell nodes from the underlying gridpane
        //and gather them for addition below
        List<Node> nodes = removeUnderlyingNodes(row);

        //add the row(nodes) in the new position
        rows.add(toPosition, row);
        this.addRow(toPosition, nodes.toArray(new Node[nodes.size()]));

        return this;
    }

    private List<Node> removeUnderlyingNodes(Row row) {
        int cellCount = row.cells.size();
        List<Node> nodes = new ArrayList<>(cellCount);
        for(Row.Cell cell : row.cells) {
            getChildren().remove(cell.node);
            nodes.add(cell.node);
        }
        return nodes;
    }

    private List<Node> getNodesForRow(Row row) {
        List<Node> nodes = new ArrayList<>(row.cells.size());
        nodes.addAll(row.cells.stream().map(cell -> cell.node).collect(Collectors.toList()));
        return nodes;
    }

    private List<Row> rows = new LinkedList<>();

    public static class Row {

        public Row(String key) {
            id = UUID.randomUUID();
            this.key = key;
        }

        public Row cells(Node... nodes)  {
            for(Node node : nodes) {
                cells.add(new Cell(node));
            }
            return this;
        }

        public Row cell(Node node) {
            cells.add(new Cell(node));
            return this;
        }

        private List<Cell> cells = new ArrayList<>();

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Row row = (Row) o;
            return id.equals(row.id);
        }

        @Override
        public int hashCode() {
            return id.hashCode();
        }

        public final UUID id;
        public final String key;

        public static class Cell {

            public Cell(Node node) {
                this.node = node;
            }
            private final Node node;
        }
    }
}
