package org.jbo.busy.tools.ui;

import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.scene.Node;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;


public abstract class MaskedPane extends StackPane {

    public MaskedPane() {
        maskPane = new Pane();
        maskPane.setBackground(new Background(new BackgroundFill(new Color(0, 0, 0, 0.5), null, null)));
        maskPane.setVisible(false);

        progress = new ProgressIndicator();
        progress.setVisible(false);
    }

    public MaskedPane setNodetoMask(Node nodeToMask) {
        getChildren().addAll(nodeToMask, maskPane, progress);
        return this;
    }

    public void bindWait(ReadOnlyDoubleProperty progressValue) {
        maskPane.setVisible(true);
        progress.progressProperty().bind(progressValue);
        progress.setVisible(true);
    }

    public void showWait() {
        progress.setProgress(-1.0);
        maskPane.setVisible(true);
        progress.setVisible(true);
    }

    public void hideWait() {
        progress.setVisible(false);
        progress.progressProperty().unbind();
        maskPane.setVisible(false);
    }

    private Pane maskPane;
    private ProgressIndicator progress;
}
