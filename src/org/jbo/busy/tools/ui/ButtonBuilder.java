package org.jbo.busy.tools.ui;


import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.TextAlignment;
import org.apache.commons.lang.StringUtils;
import org.controlsfx.glyphfont.Glyph;

public class ButtonBuilder {

    public static Button createTextButton(String text, String tooltip, EventHandler<ActionEvent> handler) {
        return createButton(text, null, null, tooltip, handler);
    }

    public static Button createButton(String text, TextAlignment alignment, Image image,
                               String tooltip, EventHandler<ActionEvent> handler) {
        Button button = new Button();

        addText(button, text, alignment);
        addTooltip(button, tooltip);

        if(image != null) {
            button.setGraphic(new ImageView(image));
            button.setPadding(new Insets(image.getWidth() / 4));
        }

        addHandler(button, handler);

        return button;
    }

    public static Button createImageButton(Glyph icon, String tooltip, EventHandler<ActionEvent> handler) {
        Button button = new Button("", icon);
        button.setPadding(new Insets(icon.getFontSize() / 4));
        addTooltip(button, tooltip);
        addHandler(button, handler);
        return button;
    }

    private static void addTooltip(Button button, String tooltip) {
        if(!StringUtils.isEmpty(tooltip)) {
            button.setTooltip(new Tooltip(tooltip));
        }
    }

    private static void addHandler(Button button, EventHandler<ActionEvent> handler) {
        if(handler == null) {
            throw new RuntimeException("A button without a handler?");
        }
        button.setOnAction(handler);
    }

    private static void addText(Button button, String text, TextAlignment alignment) {
        if(text != null) {
            button.setText(text);

            if(alignment != null) {
                button.setTextAlignment(alignment);
            }
        }
    }
}
