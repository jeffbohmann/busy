package org.jbo.busy.tools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 */
public class Log {

    public static Log get() {
        //get a stack trace from this thread and try to find the calling class'
        //type to initialize the logger with
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        //look up through the stack for the first reference to a <clinit> method,
        //which will be the static init call to this method
        String foundClass = null;
        for(StackTraceElement stackElement : stackTrace) {
            if(stackElement.getMethodName().toLowerCase().equals(CLINIT)) {
                //found it, now get the class' name
                foundClass = stackElement.getClassName();
                break;
            }
        }

        //make sure we have a class figured out. If not
        //it wasn't initialized statically, which is not efficient anyway, so complain
        if(foundClass == null) {
            throw new RuntimeException("Hey, if you use " + Log.class.getCanonicalName() + ", make sure it's " +
                   "from a static context so it's efficient and" +
                   " the owning class can be detected automatically for you :)");
        }

        //make a new Log wrapper and send it back
        return new Log(LoggerFactory.getLogger(foundClass));
    }

    public void debug(String message) {
        logger.debug(message);
    }
    public void debug(String message, Object... toStringify) {
        logger.debug(message, toStringify);
    }
    public void info(String message) {
        logger.info(message);
    }
    public void info(String message, Object... toStringify) {
        logger.info(message, toStringify);
    }
    public void warn(String message) { logger.warn(message);}
    public void warn(String message, Throwable throwable) {
        logger.warn(message, throwable);
    }
    public void error(String message, Throwable throwable) {
        logger.error(message, throwable);
    }

    private Log(Logger logger) {
        this.logger = logger;
    }
    private Logger logger;
    private static String CLINIT = "<clinit>";
}
