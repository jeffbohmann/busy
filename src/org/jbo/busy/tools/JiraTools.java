package org.jbo.busy.tools;

import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.RestClientException;
import com.atlassian.jira.rest.client.api.domain.Issue;
import com.atlassian.jira.rest.client.api.domain.User;
import com.atlassian.jira.rest.client.api.domain.input.WorklogInputBuilder;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import com.atlassian.util.concurrent.Promise;
import net.rcarz.jiraclient.BasicCredentials;
import net.rcarz.jiraclient.JiraClient;
import net.rcarz.jiraclient.JiraException;
import net.rcarz.jiraclient.WorkLog;
import org.jbo.busy.file.ConfigFile;
import org.jbo.busy.file.YamlTranslator;
import org.joda.time.DateTime;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;


public class JiraTools {

    public JiraTools(YamlTranslator<ConfigFile> configYamlTranslator) {
        this.configYamlTranslator = configYamlTranslator;
    }

    public void ensureSession() throws IssueSystemException {
        if (client != null) {
            try {
                client.close();
            }
            catch(IOException e) {
                log.warn("Failed to close client connection.", e);
            }
        }

        ConfigFile configFile = configYamlTranslator.read();
        try {
            client = new AsynchronousJiraRestClientFactory().createWithBasicHttpAuthentication(
                    new URI(configFile.getBaseUrl()), configFile.getUser(), configFile.getDecodedPassword());
        }
        catch(URISyntaxException e) {
            throw IssueSystemException.forReason("URI failure:", e, IssueSystemException.Reason.INVALID_URI);
        }
        catch (Exception e) {
            String msg = "Failed to authenticate.";
            log.error(msg, e);
            throw IssueSystemException.forReason(msg, e, IssueSystemException.Reason.AUTH_FAILED_401);
        }
    }

    public Issue getIssue(String issueKey) throws IssueSystemException {
        return retry.twice((key, nullo1, nullo2) -> {
            Promise<Issue> promise = client.getIssueClient().getIssue(key);
            return promise.claim();
        }, issueKey, null, null);

    }

    public void getIssue(String issueKey, Function<Issue,Void> callback) throws IssueSystemException {
        retry.twice((key, callme, nullo) -> {
            Promise<Issue> promise = client.getIssueClient().getIssue(key);
            promise.done(callme::apply);
            return null;
        }, issueKey, callback, null);
    }

    public void logWork(Issue issue, Integer minutesToLog, String comment) throws IssueSystemException {
        retry.twice((inIssue, inMinutesToLog, inComment) -> {
            Promise<Void> result = client.getIssueClient().addWorklog(inIssue.getWorklogUri(),
                    new WorklogInputBuilder(inIssue.getSelf())
                            .setMinutesSpent(inMinutesToLog)
                            .setComment(inComment).build());
            result.claim();
            return null;
        }, issue, minutesToLog, comment);

    }

    public User getCurrentUserInfo() throws IssueSystemException {
        return retry.twice((nullo1, nullo2, nullo3) -> {
            return client.getUserClient().getUser(
                    configYamlTranslator.read().getUser()).claim();
        }, null, null, null);
    }

    public List<WorkLog> getUserWorklogs(String issueKey, DateTime after, DateTime before) throws IssueSystemException {
        ConfigFile configFile = configYamlTranslator.read();
        JiraClient jiraClient = new JiraClient(configFile.getBaseUrl(),
                new BasicCredentials(configFile.getUser(), configFile.getDecodedPassword()));
        try {
            //TODO: this jira client doesn't expose the 'started' field
            //TODO: for a worklog, which is really stupid. until then...find something else I guess.
            net.rcarz.jiraclient.Issue issue = jiraClient.getIssue(issueKey);
            List<WorkLog> worklogs = issue.getAllWorkLogs();
            return worklogs.stream().filter(
                    log ->
                            log.getAuthor().getName().equals(configFile.getUser()) //&&
                            /*log.getCreatedDate()*/)
                    .collect(Collectors.toList());
        }
        catch(JiraException e) {
            String msg = "Failure getting work logs from issue system.";
            log.error(msg, e);
            throw IssueSystemException.fromCause(msg, e);
        }
    }

    private JiraRestClient client;
    private Retry retry = new Retry();
    private final YamlTranslator<ConfigFile> configYamlTranslator;

    interface TriFunction<I, J, K, O> {
        O apply(I i, J j, K k);
    }
    private class Retry {

        public <I,J,K,O> O twice(TriFunction<I,J,K,O> function, I input1, J input2, K input3) throws IssueSystemException {
            try {
                ensureSession();
                return function.apply(input1, input2, input3);
            }
            catch(Exception e) {
                log.error("Failed to get a session, retrying...", e);
                try {
                    ensureSession();
                    return function.apply(input1, input2, input3);
                }
                catch(RestClientException e2) {
                    log.error("2nd attempt getting a session failed.", e2);
                    throw IssueSystemException.fromCause("Failure ", e2);
                }
                catch(Exception e2) {
                    log.error("2nd attempt getting a session failed.", e2);
                    throw IssueSystemException.forReason("Failure ", e2, IssueSystemException.Reason.UNKNOWN);
                }
            }
        }
    }

    private static final Log log = Log.get();
}
