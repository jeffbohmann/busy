package org.jbo.busy;

import com.atlassian.jira.rest.client.api.domain.Issue;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.StageStyle;
import org.controlsfx.glyphfont.FontAwesome;
import org.jbo.busy.tools.ui.UITools;


public class TimeLogWindow extends Dialog<TimeLogWindow.Result> {
    public TimeLogWindow(Issue issue, int time) {
        super();
        initStyle(StageStyle.UTILITY);

        setHeaderText(issue.getKey() + " - " + issue.getSummary());

        initModality(Modality.APPLICATION_MODAL);
        setTitle("Log Time");

        ButtonType logButtonType = new ButtonType("Log", ButtonBar.ButtonData.FINISH);
        getDialogPane().getButtonTypes().add(logButtonType);
        getDialogPane().getButtonTypes().add(ButtonType.CANCEL);

        Button logButton = (Button)getDialogPane().lookupButton(logButtonType);
        logButton.setGraphic(UITools.glyph(FontAwesome.Glyph.CHECK).color(Color.SEAGREEN));

        GridPane fields = new GridPane();
        fields.setHgap(10);
        fields.setVgap(10);
        fields.setPadding(new Insets(15));

        TextField minutes = new TextField();
        minutes.setPromptText("Time");
        minutes.setText(String.valueOf(time));
        minutes.textProperty().addListener((observable, oldValue, newValue) -> {
            logButton.setDisable(newValue.trim().isEmpty());
        });
        fields.add(minutes, 0, 0);

        TextArea comment = new TextArea();
        comment.setPromptText("Comment");
        fields.add(comment, 0, 1, 2, 1);

        getDialogPane().setContent(fields);
        setResultConverter(param -> {
            if(param == logButtonType) {
                return new Result(Integer.parseInt(minutes.getText(), 10), comment.getText());
            }
            return null;
        });
    }

    public static class Result {
        public final int minutes;
        public final String comment;

        public Result(int minutes, String comment) {
            this.minutes = minutes;
            this.comment = comment;
        }
    }
}
