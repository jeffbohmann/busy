package org.jbo.busy.task;

import com.atlassian.jira.rest.client.api.domain.Issue;
import javafx.concurrent.Task;
import org.jbo.busy.file.IssuesFile;
import org.jbo.busy.tools.IssueSystemException;
import org.jbo.busy.tools.JiraTools;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;


public class LoadIssuesTask extends Task<List<LoadIssuesTask.Result>> {

    public LoadIssuesTask(final IssuesFile issuesFile, JiraTools jiraTools) {
        this.issuesFile = issuesFile;
        this.jiraTools = jiraTools;
    }

    @Override
    protected List<Result> call() throws Exception {
        int issueCount = issuesFile.size();
        final AtomicInteger i = new AtomicInteger(1);
        List<Result> issueData = new ArrayList<>();
        issuesFile.iterator().forEachRemaining(issue -> {
            String key = issue.key;
            try {
                Issue jiraIssue = jiraTools.getIssue(key);
                issueData.add(new Result(jiraIssue, issue.times));
            } catch (IssueSystemException e) {
                issueData.add(new Result(key, e));
            } finally {
                updateProgress(i.getAndIncrement(), issueCount);
            }
        });

        return issueData;
    }

    public static class Result {
        public Result(Issue issue, List<Integer> times) {
            jiraIssue = issue;
            this.times = times;
            key = null;
            e = null;
            failed = false;
        }
        public Result(String key, IssueSystemException e) {
            jiraIssue = null;
            times = null;
            this.key = key;
            this.e = e;
            failed = true;
        }

        public final Issue jiraIssue;
        public final List<Integer> times;
        public final boolean failed;
        public final String key;
        public final IssueSystemException e;
    }

    private final IssuesFile issuesFile;
    private final JiraTools jiraTools;
}
