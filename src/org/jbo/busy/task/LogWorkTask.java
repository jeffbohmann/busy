package org.jbo.busy.task;

import com.atlassian.jira.rest.client.api.RestClientException;
import com.atlassian.jira.rest.client.api.domain.Issue;
import javafx.concurrent.Task;
import org.jbo.busy.tools.JiraTools;


public class LogWorkTask extends Task<Boolean> {

    public LogWorkTask(final JiraTools jiraTools, final Issue jiraIssue, final int minutes, final String comment) {
        this.jiraTools = jiraTools;
        this.jiraIssue = jiraIssue;
        this.minutes = minutes;
        this.comment = comment;
    }

    @Override
    protected Boolean call() throws Exception {
        try {
            jiraTools.logWork(jiraIssue, minutes, comment);
            return true;
        }
        catch(RestClientException e) {
            setException(e);
            return false;
        }
    }

    private final JiraTools jiraTools;
    private final Issue jiraIssue;
    private final int minutes;
    private final String comment;
}
