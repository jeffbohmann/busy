package org.jbo.busy;

import com.atlassian.jira.rest.client.api.domain.Issue;
import com.atlassian.jira.rest.client.api.domain.User;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.apache.commons.lang.StringUtils;
import org.jbo.busy.file.ConfigFile;
import org.jbo.busy.file.IssuesFile;
import org.jbo.busy.file.YamlTranslator;
import org.jbo.busy.file.YamlTranslator.TypeMapping;
import org.jbo.busy.tools.IssueSystemException;
import org.jbo.busy.tools.JiraTools;
import org.jbo.busy.tools.Log;

import java.util.ArrayList;

/**
 * Main window
 */
public class Busy extends Application  {

    public static void main(String... args) {
        launch(args);
    }

    public static Busy app() {return me;}

    public void start(Stage stage) throws Exception {
        me = this;
        log.info("Getting busy...");
        log.debug("Initializing configuration file...");
        configYamlTranslator = new YamlTranslator.Build<ConfigFile>()
                .rootIsType(ConfigFile.class)
                .rootTagIs("config")
                .forFile("./cfg/config.yaml")
                .get();

        log.debug("Initializing issues file...");
        YamlTranslator<IssuesFile> issuesYamlTranslator = new YamlTranslator.Build<IssuesFile>()
                .rootIsType(IssuesFile.class)
                .rootTagIs("issues")
                .addTag(IssuesFile.Issue.class, "issue")
                .addTag(ArrayList.class, "times")
                .rootIsCollectionOf(IssuesFile.Issue.class)
                .mapTypes(TypeMapping.newCollectionMapping(IssuesFile.Issue.class, "times", Integer.class))
                .forFile("./cfg/issues.yaml")
                .get();

        log.debug("Creating tools...");
        jiraTools = new JiraTools(configYamlTranslator);

        log.debug("Creating UI components...");
        MainToolbar mainToolbar = new MainToolbar()
                .on(MainToolbar.Action.REFRESH, refreshEvent -> refresh(false, true))
                .on(MainToolbar.Action.CONFIG, configEvent -> showConfigWindow())
                .on(MainToolbar.Action.FONT_SIZE_INC, event -> adjustFontSize(1))
                .on(MainToolbar.Action.FONT_SIZE_DEC, event -> adjustFontSize(-1));

        //set this up first in case anyone needs to set status messages
        statusBar = new StatusBar();

        actionToolbar = new ActionToolbar(jiraTools);
        actionToolbar.on(ActionToolbar.Action.ISSUE_ENTERED, event -> {
            Issue issue = (Issue)event.getSource();
            IssuesFile issuesFile = issuesYamlTranslator.read();
            if(issuesFile == null) {
                //empty file, so make a new one
                issuesFile = new IssuesFile();
            }
            issuesFile.add(new IssuesFile.Issue(issue.getKey(), 15, 30, 60));
            issuesYamlTranslator.write(issuesFile);
            refresh(false, true);
        });

        issuesArea = new IssuesArea(jiraTools, configYamlTranslator, issuesYamlTranslator);
        VBox.setVgrow(issuesArea, Priority.ALWAYS);

        BorderPane mainPane = new BorderPane();
        mainPane.setCenter(new VBox(actionToolbar, issuesArea));
        mainPane.setTop(mainToolbar);
        mainPane.setBottom(statusBar);

        Scene theScene = new Scene(mainPane);
        theScene.getStylesheets().add(
                getClass().getResource("/org/jbo/busy/css/busy.css").toExternalForm()
        );

        stage.setWidth(640);
        stage.setHeight(480);
        stage.setTitle("Busy");
        stage.setScene(theScene);
        stage.getIcons().setAll(
                new Image(getClass().getResourceAsStream("/org/jbo/busy/img/beaverhead128.png")),
                new Image(getClass().getResourceAsStream("/org/jbo/busy/img/beaverhead64.png")),
                new Image(getClass().getResourceAsStream("/org/jbo/busy/img/beaverhead32.png")),
                new Image(getClass().getResourceAsStream("/org/jbo/busy/img/beaverhead16.png"))
        );
        stage.show();

        log.info("Loading configuration...");
        ConfigFile configFile = configYamlTranslator.read();
        if(configFile == null ||
                StringUtils.isEmpty(configFile.getBaseUrl()) ||
                StringUtils.isEmpty(configFile.getUser())) {
            log.info("Configuration required, showing settings.");
            showConfigWindow();
        }
        else {
            log.info("Fetching issues...");
            refresh(true, true);
        }

        //keep the add issue field from having focus (initially).
        // This allows the prompt text to show.
        mainPane.requestFocus();
    }

    public void status(String message) {
        statusBar.setStatus(message);
    }

    public void handleIssueSystemException(IssueSystemException e) {
        String msg = e.getMessage();
        msg = msg.length() > 1200 ? msg.substring(0, 1200) + "..." : msg;
        switch(e.why()) {
            case INVALID_URI:
                status("Invalid server URL");
                showConfigWindow();
                showError("Please check the base server url for correctness. " + msg);
                break;
            case AUTH_FAILED_401:
                status("Invalid credentials.");
                statusBar.updateUserInfo("Not logged in.", null);
                showConfigWindow();
                showError("Failed to log in. Please check your credentials. \nMaybe you've got a captcha to solve?");
                break;
            case FORBIDDEN_403:
                status("Insufficient permission.");
                statusBar.updateUserInfo("Not authorized", null);
                showConfigWindow();
                showError("The current credentials do not provide access. \nPlease check user account permissions.");
                break;
            case UNKNOWN:
                status("An unknown error occurred.");
                showError("Not sure what went wrong, but here it is: \n" + msg);
                break;
            default:
                showError("Let someone know about this: [" + e.getClass().getName() + "]\n" + msg);
        }
    }

    private void refresh(boolean authenticate, boolean queryIssueSystem) {
        if(authenticate) {
            log.debug("Authenticating with issue system...");
            try {
                jiraTools.ensureSession();
            }
            catch(IssueSystemException e)  {
                log.error("Error while authenticating.", e);
                handleIssueSystemException(e);
                return;
            }
            log.debug("Authentication successful. Loading user details...");
            updateUserDisplay();
        }

        if(queryIssueSystem) {
            log.debug("Reloading issues from remote server...");
            issuesArea.reload();
        }
        else {
            log.debug("Refreshing issues list...");
            issuesArea.refresh();
        }
    }

    private void updateUserDisplay() {
        try {
            User user = jiraTools.getCurrentUserInfo();
            statusBar.updateUserInfo(user.getDisplayName(),
                    user.getSmallAvatarUri().toASCIIString().replace("&s=16", "&s=20"));
        }
        catch(IssueSystemException e) {
            log.error("Error getting user information.", e);
            statusBar.updateUserInfo("User info not available.", null);
            handleIssueSystemException(e);
        }
    }

    private void adjustFontSize(int amount) {
        ConfigFile configFile = configYamlTranslator.read();
        int currentSize = configFile.getFontSize();
        int newSize = currentSize + amount;

        //limit to meaningful font sizes
        if(newSize == 0) {
            return;
        }

        configFile.setFontSize(newSize);
        configYamlTranslator.write(configFile);
        refresh(false, false);
    }

    private void showConfigWindow() {
        ConfigWindow cw = new ConfigWindow(configYamlTranslator)
                .on(ConfigWindow.Action.SAVE, saveEvent -> refresh(true, true));
        cw.centerOnScreen();
        cw.show();
    }

    private void showError(String msg) {
        log.warn(msg);
        if(errorDialog == null) {
            errorDialog = new Alert(Alert.AlertType.ERROR);
            errorDialog.setWidth(500);
            errorDialog.setHeight(600);
        }

        if(errorDialog.isShowing()) {
            errorDialog.setContentText(errorDialog.getContentText() + "\n\n" + msg);
        }
        else {
            errorDialog.setContentText(msg);
            errorDialog.show();
        }
    }

    private static Busy me;

    private static JiraTools jiraTools;
    private static YamlTranslator<ConfigFile> configYamlTranslator;
    private static ActionToolbar actionToolbar;
    private static IssuesArea issuesArea;
    private static StatusBar statusBar;
    private static Alert errorDialog;
    private static final Log log = Log.get();
}
