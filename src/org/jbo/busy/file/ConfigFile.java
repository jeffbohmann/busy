package org.jbo.busy.file;

import java.util.Base64;


public class ConfigFile {

    public ConfigFile() {
        fontSize = DEFAULT_FONT_SIZE;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPasswordToEncode(String password) {
        this.password = new String(Base64.getEncoder().encode(password.getBytes()));
    }
    public String getDecodedPassword() {
        if(password == null ||
                password.trim().isEmpty()) {
            return "";
        }
        return new String(Base64.getDecoder().decode(password));
    }

    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    private String baseUrl;
    private String user;
    private String password;
    private int fontSize;

    private static final int DEFAULT_FONT_SIZE = 12;
}
