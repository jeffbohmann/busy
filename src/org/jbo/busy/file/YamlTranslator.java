package org.jbo.busy.file;

import com.esotericsoftware.yamlbeans.YamlConfig;
import com.esotericsoftware.yamlbeans.YamlReader;
import com.esotericsoftware.yamlbeans.YamlWriter;
import org.jbo.busy.tools.Log;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class YamlTranslator<T> {

    private final Path path;
    private final Class<T> rootType;
    private final String rootTag;
    private final List<TypeMapping> typeMappings;
    private final Map<Class<?>,String> tagMappings;
    private final Class<?> rootCollectionElementType;

    private YamlTranslator(Build<T> builder) {
        path = builder.path;
        rootType = builder.rootType;
        rootTag = builder.rootTag;
        typeMappings = builder.typeMappings;
        tagMappings = builder.tagMappings;
        rootCollectionElementType = builder.rootCollectionElementType;

        if(!Files.exists(path)) {
            //create an empty path to use
            try {
                Files.createDirectories(path.getParent());
                Files.createFile(path);
                write(rootType.newInstance());
            }
            catch(IOException|InstantiationException|IllegalAccessException e) {
                throw new RuntimeException("Failed to create file at path[" + path + "].", e);
            }
        }

        //check read/write on path
        if(!Files.isReadable(path)) {
            throw new RuntimeException("No permissions to read from path[" + path + "].");
        }
        if(!Files.isWritable(path)) {
            throw new RuntimeException("No permissions to write to path[" + path + "].");
        }
    }

    public T read() {
        T object;
        try {
            YamlReader reader = new YamlReader(new FileReader(path.toFile()));
            YamlConfig config = reader.getConfig();
            configureTagMappings(config);
            configureTypeMappings(config);

            if(rootCollectionElementType != null) {
                object = reader.read(rootType, rootCollectionElementType);
            }
            else {
                object = reader.read(rootType);
            }
            reader.close();
        }
        catch(IOException e) {
            String msg = "Failure reading from path[" + path + "].";
            log.error(msg, e);
            throw new RuntimeException(msg, e);
        }
        return object;
    }

    public void write(T object) {
        try {
            YamlWriter writer = new YamlWriter(new FileWriter(path.toFile()));
            YamlConfig config = writer.getConfig();
            configureTagMappings(config);
            configureTypeMappings(config);
            writer.write(object);
            writer.close();
        }
        catch(IOException e) {
            String msg = "Failed to write path[" + path + "].";
            log.error(msg, e);
            throw new RuntimeException(msg, e);
        }
    }

    private void configureTagMappings(YamlConfig config) {
        config.setClassTag(rootTag, rootType);

        if(tagMappings == null) { return; }

        for(Class<?> type : tagMappings.keySet()) {
            config.setClassTag(tagMappings.get(type), type);
        }
    }

    private void configureTypeMappings(YamlConfig config) {
        if(typeMappings == null) { return; }

        for(TypeMapping typeMapping : typeMappings) {
            if(typeMapping.mappingType == TypeMapping.Type.ELEMENT) {
                config.setPropertyElementType(
                        typeMapping.owner, typeMapping.field, typeMapping.type);
            }
            else if(typeMapping.mappingType == TypeMapping.Type.PROPERTY) {
                config.setPropertyDefaultType(
                        typeMapping.owner, typeMapping.field, typeMapping.type);
            }
        }
    }

    public static class Build<T> {

        public Build<T> rootIsType(Class<T> rootType) {
            this.rootType = rootType;
            return this;
        }

        public Build<T> rootTagIs(String rootTag) {
            this.rootTag = rootTag;
            return this;
        }

        public Build<T> forFile(String filePath) {
            this.path = Paths.get(filePath);
            return this;
        }

        public Build<T> rootIsCollectionOf(Class<?> collectionElementType) {
            this.rootCollectionElementType = collectionElementType;
            return this;
        }

        public YamlTranslator<T> get() {
            return new YamlTranslator<>(this);
        }

        public Build<T> addTag(Class<?> type, String tag) {
            tagMappings.put(type, tag);
            return this;
        }

        public Build<T> mapTypes(TypeMapping... typeMappings) {
            this.typeMappings = Arrays.asList(typeMappings);
            return this;
        }

        private Path path;
        private Class<T> rootType;
        private String rootTag;
        private List<TypeMapping> typeMappings;
        private Map<Class<?>,String> tagMappings = new HashMap<>();
        private Class<?> rootCollectionElementType;
    }

    public static class TypeMapping {

        public static TypeMapping newCollectionMapping(Class<?> owningClass, String collectionPropertyName, Class<?> collectionElementType) {
            return new TypeMapping(Type.ELEMENT, owningClass, collectionPropertyName, collectionElementType);
        }

        public static TypeMapping newPropertyMapping(Class<?> parentType, String propertyName, Class<?> propertyType) {
            return new TypeMapping(Type.PROPERTY, parentType, propertyName, propertyType);
        }

        private TypeMapping(Type mappingType, Class<?> owner, String field, Class<?> type) {
            this.mappingType = mappingType;
            this.owner = owner;
            this.field = field;
            this.type = type;
        }
        private Type mappingType;
        private Class<?> owner;
        private String field;
        private Class<?> type;

        private enum Type {
            PROPERTY, ELEMENT
        }
    }

    private static final Log log = Log.get();
}
