package org.jbo.busy.file;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class IssuesFile extends ArrayList<IssuesFile.Issue> {

    public static class Issue {
        public Issue(){}
        public Issue(String key, Integer... times) {
            this.key = key;
            this.times = Arrays.asList(times);
        }
        public String key;
        public List<Integer> times;
    }
}
