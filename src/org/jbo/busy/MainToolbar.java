package org.jbo.busy;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.ToolBar;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Color;
import org.controlsfx.glyphfont.FontAwesome;
import org.jbo.busy.tools.ui.ActionHandler;
import org.jbo.busy.tools.ui.ButtonBuilder;
import org.jbo.busy.tools.ui.DualButton;
import org.jbo.busy.tools.ui.UITools;


public class MainToolbar extends ToolBar {

    public MainToolbar() {

        setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, null, null)));
        setPadding(new Insets(3));
        setEffect(new DropShadow(10, Color.BLACK));

        getItems().add(
                ButtonBuilder.createImageButton(UITools.glyph(FontAwesome.Glyph.REFRESH).size(32), "Refresh",
                        event -> actionHandler.notify(Action.REFRESH, null)));

        getItems().add(DualButton.Build.upDown(
                UITools.glyph(FontAwesome.Glyph.PLUS).size(16), "Zoom In",
                event -> actionHandler.notify(Action.FONT_SIZE_INC, null),
                UITools.glyph(FontAwesome.Glyph.MINUS).size(16), "Zoom Out",
                event -> actionHandler.notify(Action.FONT_SIZE_DEC, null)
        ));

        getItems().add(
                ButtonBuilder.createImageButton(UITools.glyph(FontAwesome.Glyph.WRENCH).size(32), "Configuration",
                        event -> actionHandler.notify(Action.CONFIG, null)));
    }

    public MainToolbar on(Action action, EventHandler<Event> handler) {
        actionHandler.register(action, handler);
        return this;
    }

    private ActionHandler<Action> actionHandler = new ActionHandler<>();

    public enum Action {
        REFRESH,
        CONFIG,
        FONT_SIZE_INC,
        FONT_SIZE_DEC
    }
}
