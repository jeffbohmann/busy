package org.jbo.busy;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Pair;
import org.jbo.busy.file.ConfigFile;
import org.jbo.busy.file.YamlTranslator;
import org.jbo.busy.tools.ui.ActionHandler;
import org.jbo.busy.tools.ui.ButtonBuilder;

import java.util.ArrayList;
import java.util.List;


public class ConfigWindow extends Stage {

    public ConfigWindow(YamlTranslator<ConfigFile> configTranslator) {
        this.configTranslator = configTranslator;
        initModality(Modality.APPLICATION_MODAL);
        setTitle("Configuration");
        setScene(new Scene(createMainPane()));
    }

    public ConfigWindow on(Action action, EventHandler<Event> handler) {
        actionHandler.register(action, handler);
        return this;
    }

    private Pane createMainPane() {
        BorderPane main = new BorderPane();
        main.setCenter(createFieldsPane());
        main.setBottom(createButtonPane());
        return main;
    }

    private Pane createFieldsPane() {
        ConfigFile configFile = configTranslator.read();

        TextField url = new TextField();
        url.setText(configFile.getBaseUrl());
        fields.add(new Pair<>(new Label(URL), url));

        TextField user = new TextField();
        user.setText(configFile.getUser());
        fields.add(new Pair<>(new Label(USER), user));

        TextField password = new PasswordField();
        password.setText(configFile.getDecodedPassword());
        fields.add(new Pair<>(new Label(PASSWORD), password));

        GridPane fieldsPane = new GridPane();
        fieldsPane.setPadding(new Insets(10));
        fieldsPane.setHgap(10);
        fieldsPane.setVgap(10);
        for(int i = 0; i < fields.size(); i++) {
            Pair<Label,TextField> pair = fields.get(i);
            fieldsPane.addRow(i, pair.getKey(), pair.getValue());
        }
        return fieldsPane;
    }

    private Pane createButtonPane() {
        Button cancel = ButtonBuilder.createTextButton("Cancel", "Discards any changes", event -> {
            actionHandler.notify(Action.CANCEL, event);
            close();
        });
        Button save = ButtonBuilder.createTextButton("Save", "Saves any changes", event -> {
            ConfigFile configFile = new ConfigFile();
            for(Pair<Label,TextField> field : fields) {
                String value = field.getValue().getText();
                switch(field.getKey().getText()) {
                    case URL: configFile.setBaseUrl(value);break;
                    case USER: configFile.setUser(value);break;
                    case PASSWORD: configFile.setPasswordToEncode(value);break;
                }
            }

            configTranslator.write(configFile);
            actionHandler.notify(Action.SAVE, event);
            close();
        });

        FlowPane buttonsPane = new FlowPane();
        buttonsPane.getChildren().addAll(cancel, save);
        buttonsPane.setHgap(20);
        buttonsPane.setColumnHalignment(HPos.CENTER);
        return buttonsPane;
    }

    private static final String URL = "Issue Server";
    private static final String USER = "User";
    private static final String PASSWORD = "Password";

    private ActionHandler<Action> actionHandler = new ActionHandler<>();
    private List<Pair<Label,TextField>> fields = new ArrayList<>();
    private YamlTranslator<ConfigFile> configTranslator;

    public enum Action {
        SAVE, CANCEL
    }
}
